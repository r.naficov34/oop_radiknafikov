from re import A
class Book:
    ## Инициализация
    def __init__(self, title_of_the_book, writter, year_of_publicatijn, izdatel, opisanie):
        self.title_of_the_book = title_of_the_book.strip()
        self.writter = writter.strip()
        self.year_of_publicatijn = year_of_publicatijn.strip()
        self.izdatel = izdatel.strip()
        self.opisanie = opisanie.strip()
    
    ## Вывод содержимого экземпляра (вывод книги)
    def print(self):
        
        print(f"Название книги : {self.title_of_the_book}\n"
              f"Автор: {self.writter}\n"
              f"Год издания : {self.year_of_publicatijn}\n"
              f"Издатель: {self.izdatel}\n"
              f"Описание: {self.opisanie}")

    ## Возвращает название книги
    def get_title_of_the_book(self):
        return self.title_of_the_book

    ## Меняет название
    def set_title_of_the_book(self, title_of_the_book):
        self.title_of_the_book = title_of_the_book

    ## Возвращает автора
    def get_writter(self):
        return self.writter

    ## Меняет автора
    def set_writter(self, writter):
        self.writter = writter

    ## Возвращает год, в котором книга была написана
    def get_year_of_publicatijn(self):
        return self.year_of_publicatijn

    ## Меняет год
    def set_year_of_publicatijn(self, year_of_publicatijn):
        self.year_of_publicatijn = year_of_publicatijn

    ## Возвращает издателя
    def get_izdatel(self):
        return self.izdatel

    ## Меняет издателя
    def set_izdatel(self, izdatel):
        self.izdatel = izdatel

    ## Возвращает описание
    def get_opisanie(self):
        return self.opisanie

    ## Меняет описание
    def set_opisanie(self, opisanie):
        self.opisanie = opisanie
        
def get_field(help_str, old_val):
        a = input(help_str).strip()
        if a != '':
            
            return a
        return old_val

def save(libra):
  f = open('library.txt', 'w', encoding="utf-8")
  for i in libra:
    f.write(str(i.title_of_the_book)+'.'+str(i.writter)+'.'+str(i.year_of_publicatijn)+'.'+str(i.izdatel)+'.'+str(i.opisanie)+"\n")

## Создаем 1 книгу
Library = []
fLibrary = open('library.txt', encoding="utf-8")  #library-библиотека(список книг)
for line in fLibrary:
  Library.append(Book(line.split(".")[0], line.split(".")[1], line.split(".")[2], line.split(".")[3], line.split(".")[4]))
## "Рабочий" цикл. Здесь выбираем действия, которые нужно будет произвести
while True:
    print(f"Введите цифру от 1 до 6, чтобы:\n"
          f"1 - список книг\n"
          f"2 - добавить книгу\n"
          f"3 - изменить книгу\n"
          f"4 - поиск\n"
          f"5 - удалить книгу\n"
          f"6 - выход")
    n = input()
    if n == "1":
        print(f"СПИСОК КНИГ:")
        ## Вывод списка книг
        for i in range(len(Library)):
            print(f"КНИГА №{i+1}")
            Library[i].print()
    elif n == "2":
        print(f"Введите даные новой книги:")
        title_of_the_book = input("название книги: ")
        writter = input("Автор: ")
        year_of_publicatijn = input("Год публикации: ")
        izdatel = input("Издатель: ")
        opisanie = input("Описание: ")
        ## Добавление новой книги в список книг
        Library.append(Book(title_of_the_book, writter, year_of_publicatijn, izdatel, opisanie))
        save(Library)
    elif n == "3":
        print(f"Введите данные измененной книги:\n")
        book_num = int(input("Изменение книги №"))
        if book_num > len(Library):
          print("Неверная цифра")
          continue
        title_of_the_book = get_field("Название Книги(было "+Library[book_num-1].title_of_the_book+'): ', Library[book_num-1].get_title_of_the_book())
        writter = get_field("Автор(было "+Library[book_num-1].writter+'): ', Library[book_num-1].get_writter())
        year_of_publicatijn = get_field("Год Публикации(было "+Library[book_num-1].year_of_publicatijn+'): ', Library[book_num-1].get_year_of_publicatijn())
        izdatel = get_field("Издатель(было "+Library[book_num-1].izdatel+'): ', Library[book_num-1].get_izdatel())
        opisanie = get_field("Описание(было "+Library[book_num-1].opisanie+'): ', Library[book_num-1].get_opisanie())
        Library[book_num-1] = Book(title_of_the_book, writter, year_of_publicatijn, izdatel, opisanie)
        save(Library)
    elif n == "5":
        ## Удаляем книгу
        book_num = int(input("Удаление книги №"))
        if book_num > len(Library):
          print("Неверная цифра")
          continue

        del Library[book_num-1] 
        save(Library)
    elif n == "4":
        poisk = input("Введите название книги: ")
        result = filter(lambda book: poisk.lower() in book.title_of_the_book.lower(),Library)
        for i, book in enumerate(result, 1):
            print(f"КНИГА №{i}")
            book.print()
    elif n == "6":
        ## Завершение работы
        break
    else:
        print("Неверная цифра, попробуйте еще раз!")
print("Выход!")




