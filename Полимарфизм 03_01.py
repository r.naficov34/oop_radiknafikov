class LeftParagraph():
    def __init__(self, lenth):
        self.lenth = lenth
        self.array = ['']

    def add_word(self, world):
        k = 0
        if len(self.array[-1]) + len(world) <= self.lenth - 1:
            if k != 0:
                self.array[-1] += ' ' + world
            elif self.array[-1] == '':
                self.array[-1] += world
                k = 1
        else:
            self.array[-1].ljust(self.lenth)
            self.array.append(world)

    def end(self):
        if self.array[0] == "":
            del self.array[0]
        for i in self.array:
            print('{0:<{1}}'.format(i, self.lenth))
        self.array = ['']


class RightParagraph():
    def __init__(self, lenth):
        self.lenth = lenth
        self.array = ['']

    def add_word(self, world):
        k = 0
        if len(self.array[-1]) + len(world) <= self.lenth - 1:
            if k != 0:
                self.array[-1] += ' ' + world
            else:
                self.array[-1] += world
                k = 1
        else:
            self.array[-1].ljust(self.lenth)
            self.array.append(world)

    def end(self):
        if self.array[0] == "":
            del self.array[0]

        for i in self.array:
            print('{0:>{1}}'.format(i, self.lenth))

        self.array = ['']