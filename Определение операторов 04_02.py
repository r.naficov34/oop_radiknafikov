class ReversedList:
    def __init__(self, lst):
        self.lst = lst[::-1]

    def __len__(self):
        return len(self.lst)

    def __str__(self):
        """ как можно тут вывести элемент?"""
        return '{}'.format(self.lst)


        ## пример1
rl = ReversedList([10, 20, 30])
for i in range(len(rl)):
    print(rl[i])

          ## пример2
rl = ReversedList([])
print(len(rl))

          ## пример3
rl = ReversedList([10])
print(len(rl))
print(rl[0])
